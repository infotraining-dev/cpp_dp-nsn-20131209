#ifndef SHAPE_GROUP_HPP_
#define SHAPE_GROUP_HPP_

#include <list>
#include <boost/bind.hpp>
#include "shape.hpp"
#include "clone_factory.hpp"

namespace Drawing
{

// TO DO: zaimplementowac kompozyt grupuj�cy kszta�ty geometryczne
class ShapeGroup : public Shape
{
    std::list<Shape*> shapes_;
public:
    ShapeGroup() {}

    ShapeGroup(const ShapeGroup& source)
    {
        std::transform(source.shapes_.begin(), source.shapes_.end(),
                       std::back_inserter(shapes_), boost::bind(&Shape::clone, _1));
    }

    ShapeGroup& operator=(const ShapeGroup& other)
    {
        ShapeGroup temp;
        swap(temp);

        return *this;
    }

    void swap(ShapeGroup& other)
    {
        shapes_.swap(other.shapes_);
    }

    void add(Shape* shape)
    {
        shapes_.push_back(shape);
    }

    void draw() const
    {
        std::for_each(shapes_.begin(), shapes_.end(), boost::bind(&Shape::draw, _1));
    }

    void move(int dx, int dy)
    {
        std::for_each(shapes_.begin(), shapes_.end(), boost::bind(&Shape::move, _1, dx, dy));
    }

    ShapeGroup* clone() const
    {
        return new ShapeGroup(*this);
    }

    void read(std::istream &in)
    {
        int count;
        in >> count;

        for(int i = 0; i < count; ++i)
        {
            std::string type_id;
            in >> type_id;

            Shape* shape = ShapeFactory::instance().create(type_id);
            shape->read(in);

            add(shape);
        }
    }

    void write(std::ostream &out)
    {
        std::for_each(shapes_.begin(), shapes_.end(),
                      boost::bind(&Shape::write, _1, boost::ref(out)));
    }
};

}

#endif /*SHAPE_GROUP_HPP_*/
