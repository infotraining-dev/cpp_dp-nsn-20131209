#include <iostream>

using namespace std;

template <typename T>
class Collection
{
    struct Node
    {
        Node(const T& value) : value_(value), next_(NULL)
        {}

        T value_;
        Node* next_;
    };

    Node* head_;
    Node* last_;
public:
    Collection() : head_(NULL), last_(NULL)
    {
    }

    void add(const T& item)
    {
        Node* new_node = new Node(item);

        if (!head_)
        {
            head_ = new_node;
            last_ = new_node;
        }
        else
        {
            last_->next_ = new_node;
            last_ = new_node;
        }
    }

    T& front()
    {
        return head_->value_;
    }

    const T& front() const
    {
        return head_->value_;
    }

    T& back()
    {
        return last_->value_;
    }

    const T& back() const
    {
        return last_->value_;
    }
};

int main()
{
    Collection<int> coll1;

    coll1.add(1);
    coll1.add(2);
    coll1.add(3);

    cout << coll1.front() << " " << coll1.back() << endl;
}

