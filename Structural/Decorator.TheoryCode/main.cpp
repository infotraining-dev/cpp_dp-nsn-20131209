#include "decorator.hpp"

void client(Component& c)
{
    c.operation();
}

int main()
{
	 // Create ConcreteComponent and two Decorators
    Component* d2 = new ConcreteDecoratorB(
                        new ConcreteDecoratorA(
                            new ConcreteComponent));

     client(*d2);

	 std::cout << std::endl;

	 delete d2;
}
