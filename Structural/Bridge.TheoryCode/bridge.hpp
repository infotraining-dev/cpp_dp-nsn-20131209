#ifndef BRIDGE_HPP_
#define BRIDGE_HPP_

#include <iostream>

class Implementor;

// "Abstraction"
class Abstraction
{
protected:
	Implementor* implementor_;
public:
	Abstraction(Implementor* impl = 0) :
		implementor_(impl)
	{
	}

	void set_implementor(Implementor* impl)
	{
		implementor_ = impl;
	}

	virtual void operation();

	virtual ~Abstraction()
	{
	}
};

// "Implementor" 
class Implementor
{
public:
	virtual void operation() = 0;
	virtual ~Implementor() {};
};

// "RefinedAbstraction"
class RefinedAbstraction : public Abstraction
{
public:
	void operation()
	{
		implementor_->operation();
	}
};

// "ConcreteImplementorA"
class ConcreteImplementorA : public Implementor
{
public:
	void operation()
	{
		std::cout <<"ConcreteImplementorA Operation" << std::endl;
	}
};

// "ConcreteImplementorB"
class ConcreteImplementorB : public Implementor
{
public:
	void operation()
	{
		std::cout <<"ConcreteImplementorB Operation" << std::endl;
	}
};

void Abstraction::operation()
{
	implementor_->operation();
}

#endif /*BRIDGE_HPP_*/
