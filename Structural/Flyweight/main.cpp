#include <iostream>
#include <string>
#include <boost/flyweight.hpp>

using namespace std;

class Platnik
{
    boost::flyweight<string> imie_;
    boost::flyweight<string> nazwisko_;
    int id_;
public:
    Platnik(int id, const string& imie, const string& nazwisko) : imie_(imie), nazwisko_(nazwisko), id_(id)
    {
    }

    string imie() const
    {
        return imie_;
    }

    void set_imie(const std::string& imie)
    {
        imie_ = imie;
    }

    string nazwisko() const
    {
        return nazwisko_;
    }

    void set_nazwisko(const string& nazwisko)
    {
        nazwisko_ = nazwisko;
    }

    int id() const
    {
        return id_;
    }

    bool operator==(const Platnik& other) const
    {
        return id_ == other.id_ && nazwisko_ == other.nazwisko_;
    }
};

int main()
{
    Platnik osoby[] = { Platnik(1, "Jan", "Kowalski"),
        Platnik(2, "Jan", "Nowak"),
        Platnik(3, "Jan", "Marek"),
        Platnik(4, "Jan", "Nijaki") };

    for(int i = 0; i < 4; ++i)
        cout << osoby[i].imie() << " " << osoby[i].nazwisko() << endl;
}
