# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/developer/Szkolenie/CPP_DP 20131209/Structural/Proxy.Exercise/application.cpp" "/home/developer/Szkolenie/CPP_DP 20131209/Structural/Proxy.Exercise/build/CMakeFiles/Proxy.Exercise.dir/application.cpp.o"
  "/home/developer/Szkolenie/CPP_DP 20131209/Structural/Proxy.Exercise/circle.cpp" "/home/developer/Szkolenie/CPP_DP 20131209/Structural/Proxy.Exercise/build/CMakeFiles/Proxy.Exercise.dir/circle.cpp.o"
  "/home/developer/Szkolenie/CPP_DP 20131209/Structural/Proxy.Exercise/image.cpp" "/home/developer/Szkolenie/CPP_DP 20131209/Structural/Proxy.Exercise/build/CMakeFiles/Proxy.Exercise.dir/image.cpp.o"
  "/home/developer/Szkolenie/CPP_DP 20131209/Structural/Proxy.Exercise/line.cpp" "/home/developer/Szkolenie/CPP_DP 20131209/Structural/Proxy.Exercise/build/CMakeFiles/Proxy.Exercise.dir/line.cpp.o"
  "/home/developer/Szkolenie/CPP_DP 20131209/Structural/Proxy.Exercise/main.cpp" "/home/developer/Szkolenie/CPP_DP 20131209/Structural/Proxy.Exercise/build/CMakeFiles/Proxy.Exercise.dir/main.cpp.o"
  "/home/developer/Szkolenie/CPP_DP 20131209/Structural/Proxy.Exercise/rectangle.cpp" "/home/developer/Szkolenie/CPP_DP 20131209/Structural/Proxy.Exercise/build/CMakeFiles/Proxy.Exercise.dir/rectangle.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
