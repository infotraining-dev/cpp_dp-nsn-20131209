# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = "/home/developer/Szkolenie/CPP_DP 20131209/Creational/FactoryMethod.Exercise1"

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = "/home/developer/Szkolenie/CPP_DP 20131209/Creational/FactoryMethod.Exercise1/build"

# Include any dependencies generated for this target.
include CMakeFiles/FactoryMethod.Exercise1.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/FactoryMethod.Exercise1.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/FactoryMethod.Exercise1.dir/flags.make

CMakeFiles/FactoryMethod.Exercise1.dir/employee.cpp.o: CMakeFiles/FactoryMethod.Exercise1.dir/flags.make
CMakeFiles/FactoryMethod.Exercise1.dir/employee.cpp.o: ../employee.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report "/home/developer/Szkolenie/CPP_DP 20131209/Creational/FactoryMethod.Exercise1/build/CMakeFiles" $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/FactoryMethod.Exercise1.dir/employee.cpp.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/FactoryMethod.Exercise1.dir/employee.cpp.o -c "/home/developer/Szkolenie/CPP_DP 20131209/Creational/FactoryMethod.Exercise1/employee.cpp"

CMakeFiles/FactoryMethod.Exercise1.dir/employee.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/FactoryMethod.Exercise1.dir/employee.cpp.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E "/home/developer/Szkolenie/CPP_DP 20131209/Creational/FactoryMethod.Exercise1/employee.cpp" > CMakeFiles/FactoryMethod.Exercise1.dir/employee.cpp.i

CMakeFiles/FactoryMethod.Exercise1.dir/employee.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/FactoryMethod.Exercise1.dir/employee.cpp.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S "/home/developer/Szkolenie/CPP_DP 20131209/Creational/FactoryMethod.Exercise1/employee.cpp" -o CMakeFiles/FactoryMethod.Exercise1.dir/employee.cpp.s

CMakeFiles/FactoryMethod.Exercise1.dir/employee.cpp.o.requires:
.PHONY : CMakeFiles/FactoryMethod.Exercise1.dir/employee.cpp.o.requires

CMakeFiles/FactoryMethod.Exercise1.dir/employee.cpp.o.provides: CMakeFiles/FactoryMethod.Exercise1.dir/employee.cpp.o.requires
	$(MAKE) -f CMakeFiles/FactoryMethod.Exercise1.dir/build.make CMakeFiles/FactoryMethod.Exercise1.dir/employee.cpp.o.provides.build
.PHONY : CMakeFiles/FactoryMethod.Exercise1.dir/employee.cpp.o.provides

CMakeFiles/FactoryMethod.Exercise1.dir/employee.cpp.o.provides.build: CMakeFiles/FactoryMethod.Exercise1.dir/employee.cpp.o

CMakeFiles/FactoryMethod.Exercise1.dir/hrinfo.cpp.o: CMakeFiles/FactoryMethod.Exercise1.dir/flags.make
CMakeFiles/FactoryMethod.Exercise1.dir/hrinfo.cpp.o: ../hrinfo.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report "/home/developer/Szkolenie/CPP_DP 20131209/Creational/FactoryMethod.Exercise1/build/CMakeFiles" $(CMAKE_PROGRESS_2)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/FactoryMethod.Exercise1.dir/hrinfo.cpp.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/FactoryMethod.Exercise1.dir/hrinfo.cpp.o -c "/home/developer/Szkolenie/CPP_DP 20131209/Creational/FactoryMethod.Exercise1/hrinfo.cpp"

CMakeFiles/FactoryMethod.Exercise1.dir/hrinfo.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/FactoryMethod.Exercise1.dir/hrinfo.cpp.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E "/home/developer/Szkolenie/CPP_DP 20131209/Creational/FactoryMethod.Exercise1/hrinfo.cpp" > CMakeFiles/FactoryMethod.Exercise1.dir/hrinfo.cpp.i

CMakeFiles/FactoryMethod.Exercise1.dir/hrinfo.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/FactoryMethod.Exercise1.dir/hrinfo.cpp.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S "/home/developer/Szkolenie/CPP_DP 20131209/Creational/FactoryMethod.Exercise1/hrinfo.cpp" -o CMakeFiles/FactoryMethod.Exercise1.dir/hrinfo.cpp.s

CMakeFiles/FactoryMethod.Exercise1.dir/hrinfo.cpp.o.requires:
.PHONY : CMakeFiles/FactoryMethod.Exercise1.dir/hrinfo.cpp.o.requires

CMakeFiles/FactoryMethod.Exercise1.dir/hrinfo.cpp.o.provides: CMakeFiles/FactoryMethod.Exercise1.dir/hrinfo.cpp.o.requires
	$(MAKE) -f CMakeFiles/FactoryMethod.Exercise1.dir/build.make CMakeFiles/FactoryMethod.Exercise1.dir/hrinfo.cpp.o.provides.build
.PHONY : CMakeFiles/FactoryMethod.Exercise1.dir/hrinfo.cpp.o.provides

CMakeFiles/FactoryMethod.Exercise1.dir/hrinfo.cpp.o.provides.build: CMakeFiles/FactoryMethod.Exercise1.dir/hrinfo.cpp.o

CMakeFiles/FactoryMethod.Exercise1.dir/before.cpp.o: CMakeFiles/FactoryMethod.Exercise1.dir/flags.make
CMakeFiles/FactoryMethod.Exercise1.dir/before.cpp.o: ../before.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report "/home/developer/Szkolenie/CPP_DP 20131209/Creational/FactoryMethod.Exercise1/build/CMakeFiles" $(CMAKE_PROGRESS_3)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/FactoryMethod.Exercise1.dir/before.cpp.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/FactoryMethod.Exercise1.dir/before.cpp.o -c "/home/developer/Szkolenie/CPP_DP 20131209/Creational/FactoryMethod.Exercise1/before.cpp"

CMakeFiles/FactoryMethod.Exercise1.dir/before.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/FactoryMethod.Exercise1.dir/before.cpp.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E "/home/developer/Szkolenie/CPP_DP 20131209/Creational/FactoryMethod.Exercise1/before.cpp" > CMakeFiles/FactoryMethod.Exercise1.dir/before.cpp.i

CMakeFiles/FactoryMethod.Exercise1.dir/before.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/FactoryMethod.Exercise1.dir/before.cpp.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S "/home/developer/Szkolenie/CPP_DP 20131209/Creational/FactoryMethod.Exercise1/before.cpp" -o CMakeFiles/FactoryMethod.Exercise1.dir/before.cpp.s

CMakeFiles/FactoryMethod.Exercise1.dir/before.cpp.o.requires:
.PHONY : CMakeFiles/FactoryMethod.Exercise1.dir/before.cpp.o.requires

CMakeFiles/FactoryMethod.Exercise1.dir/before.cpp.o.provides: CMakeFiles/FactoryMethod.Exercise1.dir/before.cpp.o.requires
	$(MAKE) -f CMakeFiles/FactoryMethod.Exercise1.dir/build.make CMakeFiles/FactoryMethod.Exercise1.dir/before.cpp.o.provides.build
.PHONY : CMakeFiles/FactoryMethod.Exercise1.dir/before.cpp.o.provides

CMakeFiles/FactoryMethod.Exercise1.dir/before.cpp.o.provides.build: CMakeFiles/FactoryMethod.Exercise1.dir/before.cpp.o

# Object files for target FactoryMethod.Exercise1
FactoryMethod_Exercise1_OBJECTS = \
"CMakeFiles/FactoryMethod.Exercise1.dir/employee.cpp.o" \
"CMakeFiles/FactoryMethod.Exercise1.dir/hrinfo.cpp.o" \
"CMakeFiles/FactoryMethod.Exercise1.dir/before.cpp.o"

# External object files for target FactoryMethod.Exercise1
FactoryMethod_Exercise1_EXTERNAL_OBJECTS =

FactoryMethod.Exercise1: CMakeFiles/FactoryMethod.Exercise1.dir/employee.cpp.o
FactoryMethod.Exercise1: CMakeFiles/FactoryMethod.Exercise1.dir/hrinfo.cpp.o
FactoryMethod.Exercise1: CMakeFiles/FactoryMethod.Exercise1.dir/before.cpp.o
FactoryMethod.Exercise1: CMakeFiles/FactoryMethod.Exercise1.dir/build.make
FactoryMethod.Exercise1: CMakeFiles/FactoryMethod.Exercise1.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable FactoryMethod.Exercise1"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/FactoryMethod.Exercise1.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/FactoryMethod.Exercise1.dir/build: FactoryMethod.Exercise1
.PHONY : CMakeFiles/FactoryMethod.Exercise1.dir/build

CMakeFiles/FactoryMethod.Exercise1.dir/requires: CMakeFiles/FactoryMethod.Exercise1.dir/employee.cpp.o.requires
CMakeFiles/FactoryMethod.Exercise1.dir/requires: CMakeFiles/FactoryMethod.Exercise1.dir/hrinfo.cpp.o.requires
CMakeFiles/FactoryMethod.Exercise1.dir/requires: CMakeFiles/FactoryMethod.Exercise1.dir/before.cpp.o.requires
.PHONY : CMakeFiles/FactoryMethod.Exercise1.dir/requires

CMakeFiles/FactoryMethod.Exercise1.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/FactoryMethod.Exercise1.dir/cmake_clean.cmake
.PHONY : CMakeFiles/FactoryMethod.Exercise1.dir/clean

CMakeFiles/FactoryMethod.Exercise1.dir/depend:
	cd "/home/developer/Szkolenie/CPP_DP 20131209/Creational/FactoryMethod.Exercise1/build" && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" "/home/developer/Szkolenie/CPP_DP 20131209/Creational/FactoryMethod.Exercise1" "/home/developer/Szkolenie/CPP_DP 20131209/Creational/FactoryMethod.Exercise1" "/home/developer/Szkolenie/CPP_DP 20131209/Creational/FactoryMethod.Exercise1/build" "/home/developer/Szkolenie/CPP_DP 20131209/Creational/FactoryMethod.Exercise1/build" "/home/developer/Szkolenie/CPP_DP 20131209/Creational/FactoryMethod.Exercise1/build/CMakeFiles/FactoryMethod.Exercise1.dir/DependInfo.cmake" --color=$(COLOR)
.PHONY : CMakeFiles/FactoryMethod.Exercise1.dir/depend

