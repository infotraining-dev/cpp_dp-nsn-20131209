/*
 * square.hpp
 *
 *  Created on: 04-02-2013
 *      Author: Krystian
 */

#ifndef SQUARE_HPP_
#define SQUARE_HPP_

#include "shape.hpp"

namespace Drawing
{

// TODO: Doda� klase Square
class Square : public ShapeBase
{
public:
    Square(int x = 0, int y = 0, int size = 0)
        : ShapeBase(x, y), size_(size)
    {}

    void set_size(int size)
    {
        size_ = size;
    }

    int size() const
    {
        return size_;
    }

    void draw() const
    {
        std::cout << "Drawing square at " << point()
                  << " with size " << size_ << std::endl;
    }

    void read(std::istream &in)
    {
        Point pt;
        in >> pt;
        set_point(pt);
        in >> size_;
    }

    void write(std::ostream &out)
    {
        out << "Square " << point() << " " << size_ << std::endl;
    }

private:
    int size_;
};

}

#endif /* SQUARE_HPP_ */
