# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/developer/Szkolenie/CPP_DP 20131209/Creational/Prototype.Example/application.cpp" "/home/developer/Szkolenie/CPP_DP 20131209/Creational/Prototype.Example/build/CMakeFiles/Prototype.Example.dir/application.cpp.o"
  "/home/developer/Szkolenie/CPP_DP 20131209/Creational/Prototype.Example/circle.cpp" "/home/developer/Szkolenie/CPP_DP 20131209/Creational/Prototype.Example/build/CMakeFiles/Prototype.Example.dir/circle.cpp.o"
  "/home/developer/Szkolenie/CPP_DP 20131209/Creational/Prototype.Example/main.cpp" "/home/developer/Szkolenie/CPP_DP 20131209/Creational/Prototype.Example/build/CMakeFiles/Prototype.Example.dir/main.cpp.o"
  "/home/developer/Szkolenie/CPP_DP 20131209/Creational/Prototype.Example/rectangle.cpp" "/home/developer/Szkolenie/CPP_DP 20131209/Creational/Prototype.Example/build/CMakeFiles/Prototype.Example.dir/rectangle.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
